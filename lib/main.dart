import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';

// Services
import 'package:myh_contact_list/services/external/Rest.dart';

// Containers
import 'package:myh_contact_list/containers/Home.dart';
import 'package:myh_contact_list/containers/EditUser.dart';
import 'package:myh_contact_list/containers/CreateUser.dart';

// Store
import 'package:myh_contact_list/services/internal/store/states/AppState.dart';
import 'package:myh_contact_list/services/internal/store/reducers/AppReducers.dart';

void main() async {
  await rest.initial();
  final store =
      new Store<AppState>(appReducer, initialState: AppState.initial());
  await SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);

  return runApp(MyhContactList(store: store));
}

class MyhContactList extends StatelessWidget {
  final Store<AppState> store;

  MyhContactList({this.store});

  @override
  Widget build(BuildContext context) {
    return new StoreProvider<AppState>(
      store: store,
      child: new MaterialApp(
        title: 'MYH Contact List',
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        initialRoute: "/",
        routes: {
          "/": (BuildContext context) => prepareScreen(new Home()),
          "/edit": (BuildContext context) => prepareScreen(new EditUser()),
          "/create": (BuildContext context) => prepareScreen(new CreateUser()),
        },
      ),
    );
  }
}

// Set Container Direction
prepareScreen(Widget screen,
    [TextDirection textDirection = TextDirection.ltr]) {
  return new Directionality(textDirection: textDirection, child: screen);
}

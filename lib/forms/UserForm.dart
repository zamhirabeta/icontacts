import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

// Models
import 'package:myh_contact_list/services/internal/models/UserModel.dart';

// Widgets
import 'package:myh_contact_list/widgets/TextInput.dart';
import 'package:myh_contact_list/widgets/Button.dart';

class UserForm extends StatefulWidget {
  final User user;
  final Function onSubmit;
  final bool isLoading;

  UserForm({this.user, this.onSubmit, this.isLoading = false});

  @override
  _UserFormState createState() => _UserFormState();
}

class _UserFormState extends State<UserForm> {
  String id;
  String first_name;
  String last_name;
  String email;
  String phone_no;
  String gender = "Male";
  String date_of_birth;
  TextEditingController _birthdayTextInputController = new TextEditingController();
  GlobalKey<FormState> _formKey = new GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();
    if (widget.user != null) {
      id = widget.user.id;
      first_name = widget.user.first_name;
      last_name = widget.user.last_name;
      email = widget.user.email;
      gender = widget.user.gender;
      date_of_birth = widget.user.date_of_birth;
      var date = new DateTime.fromMillisecondsSinceEpoch(
          int.parse(widget.user.date_of_birth) * 1000);
      _birthdayTextInputController.text =
          DateFormat("yyyy/MM/dd").format(date).toString();
      phone_no = widget.user.phone_no;
    }
  }

  void _setGenderValue(String val) {
    setState(() {
      gender = val;
    });
  }

  Future _selectDate() async {
    DateTime picked = await showDatePicker(
        context: context,
        initialDate: new DateTime.now(),
        firstDate: new DateTime(1900, 1, 1),
        lastDate: new DateTime.now());
    if (picked != null)
      setState(() {
        date_of_birth = (picked.millisecondsSinceEpoch / 1000).toString();
        _birthdayTextInputController.text =
            DateFormat("yyyy/MM/dd").format(picked).toString();
      });
  }

  @override
  Widget build(BuildContext context) {
    return new Form(
      key: _formKey,
      child: new Expanded(
        child: new ListView(
          children: <Widget>[
            new TextInput(
              onSaved: (String input) => first_name = input,
              initialValue: first_name,
              validator: (String input) {
                if (input.length <= 1) return "First Name is required";
                return null;
              },
              label: "First Name",
            ),
            new TextInput(
              onSaved: (String input) => last_name = input,
              initialValue: last_name,
              validator: (String input) {
                if (input.length <= 1) return "Last Name is required";
                return null;
              },
              label: "Last Name",
            ),
            new TextInput(
              onSaved: (String input) => email = input,
              initialValue: email,
              validator: (String input) {
                if (input.length <= 1) return "Email is required";
                return null;
              },
              label: "Email",
            ),
            new TextInput(
              onSaved: (String input) => phone_no = input,
              initialValue: phone_no,
              validator: (String input) {
                if (input.length <= 1) return "Mobile is required";
                return null;
              },
              label: "Mobile",
            ),
            new Padding(
              padding: const EdgeInsets.only(top: 10),
              child: new InkWell(
                onTap: _selectDate,
                child: new IgnorePointer(
                  child: new TextFormField(
                    controller: _birthdayTextInputController,
                    validator: (String input) {
                      if (input.length <= 1) return "Birthday Date is required";
                      return null;
                    },
                    decoration: new InputDecoration(
                      labelText: "Birthday Date",
                      contentPadding: EdgeInsets.only(top: 5, bottom: 3),
                    ),
                  ),
                ),
              ),
            ),
            new Padding(
              padding: const EdgeInsets.only(top: 10),
              child: new Row(
                children: <Widget>[
                  new Text(
                    "Gender",
                    style: new TextStyle(
                      fontSize: 16,
                    ),
                  ),
                  new Expanded(
                    child: new RadioListTile(
                      value: "Male",
                      groupValue: gender,
                      title: Text("Male"),
                      onChanged: (val) {
                        print("Radio Tile pressed $val");
                        _setGenderValue(val);
                      },
                      activeColor: Colors.blue,
                      selected: false,
                    ),
                  ),
                  new Expanded(
                    child: new RadioListTile(
                      value: "Female",
                      groupValue: gender,
                      title: Text("Female"),
                      onChanged: (val) {
                        print("Radio Tile pressed $val");
                        _setGenderValue(val);
                      },
                      activeColor: Colors.blue,
                      selected: false,
                    ),
                  ),
                ],
              ),
            ),
            new Padding(
              padding: const EdgeInsets.only(top: 10),
              child: new Button(
                isLoading: widget.isLoading,
                label: "Save",
                onTap: () {
                  if (_formKey.currentState.validate()) {
                    _formKey.currentState.save();
                    var user = new User(
                      first_name: first_name,
                      last_name: last_name,
                      email: email,
                      phone_no: phone_no,
                      date_of_birth: date_of_birth,
                      gender: gender,
                    );
                    if (widget.user != null) user.id = widget.user.id;
                    // Pass FormData back using callback
                    widget.onSubmit(user);
                  }
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}

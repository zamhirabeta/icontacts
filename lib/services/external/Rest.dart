import 'dart:async';
import 'package:dio/dio.dart';

class Rest {
  Dio _http;
  String _baseUrl;
  Map<String, dynamic> _headers;

  // Make It Singleton
  static final Rest _rest = new Rest._internal();

  factory Rest() => _rest;

  Rest._internal();

  Future<Null> initial() async {
    _baseUrl = 'https://mock-rest-api-server.herokuapp.com/api/v1';
    _headers = {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    };
    BaseOptions options = new BaseOptions(
      baseUrl: _baseUrl,
      headers: _headers,
//      receiveTimeout: 5000,
//      connectTimeout: 5000,
    );
    _http = new Dio(options);
  }

  Future<Response> get(String url, {Map<String, dynamic> headers}) async {
    print('[GET]: $_baseUrl$url');
    if (headers != null) _headers.addAll(headers);
    print('[HEADERS]: $_headers');
    Options options = new Options(headers: _headers);
    try {
      return await _http.get(url, options: options);
    } on DioError catch (e) {
      throw e;
    }
  }

  Future<Response> post(String url,
      {Map<String, dynamic> headers, body}) async {
    print('[POST]: $_baseUrl$url');
    if (headers != null) _headers.addAll(headers);
    Options options = new Options(headers: _headers);
    try {
      return await _http.post(url, data: body, options: options);
    } on DioError catch (e) {
      throw e;
    }
  }

  Future<Response> put(String url, {Map<String, dynamic> headers, body}) async {
    print('[PUT]: $_baseUrl$url');
    if (headers != null) _headers.addAll(headers);
    Options options = new Options(headers: _headers);
    try {
      return await _http.put(url, data: body, options: options);
    } on DioError catch (e) {
      throw e;
    }
  }

  Future<Response> delete(String url, {Map<String, dynamic> headers}) async {
    print('[DELETE]: $_baseUrl$url');
    if (headers != null) _headers.addAll(headers);
    Options options = new Options(headers: _headers);
    try {
      return await _http.delete(url, options: options);
    } on DioError catch (e) {
      throw e;
    }
  }
}

Rest rest = new Rest();

class User {
  String id;
  String first_name;
  String last_name;
  String email;
  String gender;
  String date_of_birth;
  String phone_no;
  bool isFavorite;

  User({
    this.id,
    this.first_name,
    this.last_name,
    this.email,
    this.gender,
    this.date_of_birth,
    this.phone_no,
    this.isFavorite = false,
  });

  factory User.fromJson(Map<String, dynamic> json) {
    return User(
      id: json["id"],
      first_name: json["first_name"],
      last_name: json["last_name"],
      email: json["email"],
      gender: json["gender"],
      date_of_birth: json["date_of_birth"],
      phone_no: json["phone_no"],
      isFavorite: json["isFavorite"] != null ? json["isFavorite"] : false,
    );
  }

  toJson() {
    return {
      "id": this.id,
      "first_name": this.first_name,
      "last_name": this.last_name,
      "email": this.email,
      "gender": this.gender,
      "date_of_birth": this.date_of_birth,
      "phone_no": this.phone_no,
      "isFavorite": this.isFavorite,
    };
  }

  String fullName() {
    return this.first_name + " " + this.last_name;
  }
}

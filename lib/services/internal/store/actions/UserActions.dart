// Actions
import 'package:myh_contact_list/services/internal/models/UserModel.dart';

class GetUsersAction {
  final List<User> users;
  final bool reset;

  GetUsersAction(this.users, {this.reset = false});
}

class ToggleFavoriteListAction {
  final User user;

  ToggleFavoriteListAction(this.user);
}

enum UsersLoadingAction { LOADING, LOADED }

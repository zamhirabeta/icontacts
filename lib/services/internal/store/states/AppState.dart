import 'package:meta/meta.dart';

import 'package:myh_contact_list/services/internal/store/states/UserState.dart';

@immutable
class AppState {
  final UserState userState;

  AppState({this.userState});

  factory AppState.initial() => AppState(userState: UserState.initial());
}

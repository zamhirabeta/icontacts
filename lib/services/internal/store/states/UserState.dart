import 'package:myh_contact_list/services/internal/models/UserModel.dart';
import 'package:myh_contact_list/services/internal/store/actions/UserActions.dart';

class UserState {
  final List<User> users;
  bool isLoading;

  UserState(this.users, {this.isLoading});

  factory UserState.initial() => new UserState([], isLoading: false);

  factory UserState.setUsers(UserState state, GetUsersAction action) {
    // To clear list and load from page 1
    if (action.reset)
      return new UserState(action.users, isLoading: state.isLoading);
    state.users.addAll(action.users);
    return new UserState(state.users, isLoading: state.isLoading);
  }

  factory UserState.setLoadingState(
          UserState state, UsersLoadingAction action) =>
      new UserState(
        state.users,
        isLoading: action == UsersLoadingAction.LOADING ? true : false,
      );

  factory UserState.toggleFavoriteList(
      UserState state, ToggleFavoriteListAction action) {
    state.users[state.users.indexOf(action.user)].isFavorite == true
        ? state.users[state.users.indexOf(action.user)].isFavorite = false
        : state.users[state.users.indexOf(action.user)].isFavorite = true;
    return new UserState(state.users, isLoading: state.isLoading);
  }

  get contacts => users;
}

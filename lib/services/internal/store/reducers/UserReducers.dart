import 'package:redux/redux.dart';

import 'package:myh_contact_list/services/internal/store/states/UserState.dart';
import 'package:myh_contact_list/services/internal/store/actions/UserActions.dart';

// Reducer Functions
UserState _setUsers(UserState state, GetUsersAction action) =>
    new UserState.setUsers(state, action);

UserState _setLoadingState(UserState state, UsersLoadingAction action) =>
    new UserState.setLoadingState(state, action);

UserState _toggleFavoriteList(
        UserState state, ToggleFavoriteListAction action) =>
    new UserState.toggleFavoriteList(state, action);

// Reducer
final userStateReducer = combineReducers<UserState>([
  TypedReducer<UserState, GetUsersAction>(_setUsers),
  TypedReducer<UserState, UsersLoadingAction>(_setLoadingState),
  TypedReducer<UserState, ToggleFavoriteListAction>(_toggleFavoriteList),
]);

import 'package:myh_contact_list/services/internal/store/reducers/UserReducers.dart';
import 'package:myh_contact_list/services/internal/store/states/AppState.dart';

AppState appReducer(AppState state, action) {
  return AppState(
    userState: userStateReducer(state.userState, action),
  );
}

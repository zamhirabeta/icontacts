import 'dart:async';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';

// Services & Models
import 'package:myh_contact_list/services/external/Rest.dart';
import 'package:myh_contact_list/services/internal/models/UserModel.dart';

class UserRepository {
  // Get list of users
  Future<List<User>> getUsers({int page = 1, int row = 15}) async {
    try {
      final response = await rest.get("/user?page=$page&row=$row");
      var responseBody = response.data["data"] as List;
      List<User> users = [];
      responseBody.forEach((item) {
        users.add(User.fromJson(item));
      });
      return users;
    } on DioError catch (e) {
      throw e;
    }
  }

  // Edit a user by id
  Future<bool> editUser({@required User user}) async {
    try {
      var body = {
        "id": user.id,
        "first_name": user.first_name,
        "last_name": user.last_name,
        "email": user.email,
        "gender": user.gender,
        "date_of_birth": user.date_of_birth,
        "phone_no": user.phone_no
      };
      final response = await rest.put("/user/${user.id}", body: body);
      if (response.statusCode == 200) return true;
      return false;
    } on DioError catch (e) {
      throw e;
    }
  }

  // Delete a user by id
  Future<bool> deleteUser({@required String id}) async {
    try {
      final response = await rest.delete("/user/$id");
      if (response.statusCode == 200) return true;
      return false;
    } on DioError catch (e) {
      throw e;
    }
  }

  // Create new user
  Future<bool> createUser({@required User user}) async {
    try {
      var body = {
        "id": user.id,
        "first_name": user.first_name,
        "last_name": user.last_name,
        "email": user.email,
        "gender": user.gender,
        "date_of_birth": user.date_of_birth,
        "phone_no": user.phone_no
      };
      final response = await rest.post("/user/${user.id}", body: body);
      if (response.statusCode == 200) return true;
      return false;
    } on DioError catch (e) {
      throw e;
    }
  }
}

import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';

// Models & Repositories
import 'package:myh_contact_list/services/internal/models/UserModel.dart';
import 'package:myh_contact_list/services/internal/repositories/UserRepository.dart';
import 'package:myh_contact_list/services/internal/store/actions/UserActions.dart';
import 'package:myh_contact_list/services/internal/store/states/AppState.dart';
import 'package:myh_contact_list/widgets/Loading.dart';

// Widgets
import 'package:myh_contact_list/widgets/UsersListView.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  UserRepository _userRepository = new UserRepository();
  ScrollController _scrollController = new ScrollController();
  int _page = 1;
  int _currentIndex = 0;

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  Future<List<User>> _getUsers({int page = 1, int row = 15}) async {
    try {
      List<User> usersList = [];
      var users = await _userRepository.getUsers(page: page, row: row);
      usersList.addAll(users);
      return usersList;
    } catch (e) {
      throw e;
    }
  }

  Future<void> setOfflineUsers(List<User> usersList, {int page = 1}) async {
    SharedPreferences _prefs = await SharedPreferences.getInstance();
    int lastPage =
        _prefs.getInt("lastPage") == null ? 0 : _prefs.getInt("lastPage");
    if (lastPage <= page) {
      List<String> jsonUsersList = _prefs.getStringList("users") == null
          ? []
          : _prefs.getStringList("users");
      usersList.forEach((user) {
        jsonUsersList.add(jsonEncode(user.toJson()));
      });
      _prefs.setStringList("users", jsonUsersList);
      lastPage += 1;
      _prefs.setInt("lastPage", lastPage);
    }
  }

  Future<List<User>> getOfflineUsers() async {
    SharedPreferences _prefs = await SharedPreferences.getInstance();
    List<String> jsonUsersList = _prefs.getStringList("users") == null
        ? []
        : _prefs.getStringList("users");
    List<User> usersList = [];
    jsonUsersList.forEach((json) {
      usersList.add(User.fromJson(jsonDecode(json)));
    });
    return usersList;
  }

  Future<int> getOfflineLastPage() async {
    SharedPreferences _prefs = await SharedPreferences.getInstance();
    return _prefs.getInt("lastPage");
  }

  @override
  Widget build(BuildContext context) {
    return new StoreConnector<AppState, HomeView>(
        onInit: (store) async {
          store.dispatch(UsersLoadingAction.LOADING);
          var users = await getOfflineUsers();
          if (users.length == 0) {
            users = await _getUsers();
            await setOfflineUsers(users);
          }
          store.dispatch(new GetUsersAction(users));
          store.dispatch(UsersLoadingAction.LOADED);

          _scrollController.addListener(() async {
            if (_scrollController.position.pixels ==
                _scrollController.position.maxScrollExtent) {
              _page = await getOfflineLastPage();
              store.dispatch(UsersLoadingAction.LOADING);
              var users = await _getUsers(page: _page);
              await setOfflineUsers(users, page: _page);
              store.dispatch(new GetUsersAction(users));
              store.dispatch(UsersLoadingAction.LOADED);
            }
          });
        },
        converter: (store) => HomeView(
            state: store.state, contacts: store.state.userState.contacts),
        builder: (BuildContext context, HomeView hv) {
          return new Scaffold(
            key: _scaffoldKey,
            appBar: new AppBar(
              title: new Text("iContacts"),
              centerTitle: true,
            ),
            body: new Stack(
              children: <Widget>[
                new UsersListView(
                  scrollController: _scrollController,
                  users: _currentIndex == 0
                      ? hv.contacts
                      : hv.contacts.where((user) => user.isFavorite).toList(),
                  sk: _scaffoldKey,
                ),
                hv.state.userState.isLoading
                    ? new Container(
                        child: new Loading(),
                        color: new Color.fromRGBO(0, 0, 0, 0.6),
                      )
                    : new Container(),
              ],
            ),
            bottomNavigationBar: new BottomNavigationBar(
              onTap: (index) {
                setState(() {
                  _currentIndex = index;
                });
              },
              currentIndex: _currentIndex,
              items: [
                BottomNavigationBarItem(
                  icon: new Icon(Icons.contacts),
                  title: new Text('Contacts'),
                ),
                BottomNavigationBarItem(
                  icon: new Icon(Icons.favorite),
                  title: new Text('Favorites'),
                ),
              ],
            ),
            floatingActionButton: new FloatingActionButton(
              onPressed: () => Navigator.of(context).pushNamed("/create"),
              child: new Icon(Icons.add),
            ),
          );
        });
  }
}

class HomeView {
  final AppState state;
  List<User> contacts;

  HomeView({this.state, this.contacts});
}

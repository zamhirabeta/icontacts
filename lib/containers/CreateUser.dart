import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';

// Model & Services
import 'package:myh_contact_list/services/internal/models/UserModel.dart';
import 'package:myh_contact_list/services/internal/repositories/UserRepository.dart';

// Widget & Forms
import 'package:myh_contact_list/widgets/CSnackBar.dart';
import 'package:myh_contact_list/forms/UserForm.dart';

// Store
import 'package:myh_contact_list/services/internal/store/states/AppState.dart';
import 'package:myh_contact_list/services/internal/store/actions/UserActions.dart';

class CreateUser extends StatefulWidget {
  @override
  _CreateUserState createState() => _CreateUserState();
}

class _CreateUserState extends State<CreateUser> {
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  UserRepository _userRepository = new UserRepository();
  bool _isLoading = false;

  Future<List<User>> _getUsers({int page = 1, int row = 15}) async {
    try {
      List<User> usersList = [];
      var users = await _userRepository.getUsers(page: page, row: row);
      usersList.addAll(users);
      return usersList;
    } catch (e) {
      throw e;
    }
  }

  @override
  Widget build(BuildContext context) {
    return new StoreConnector<AppState, CreateUserView>(
      converter: (store) => CreateUserView(
        state: store.state,
        setUsers: (users) => store.dispatch(
          new GetUsersAction(users, reset: true),
        ),
        setLoadingState: (loadingState) => store.dispatch(
          loadingState ? UsersLoadingAction.LOADING : UsersLoadingAction.LOADED,
        ),
      ),
      builder: (BuildContext context, CreateUserView createUserView) {
        return new Scaffold(
          key: _scaffoldKey,
          appBar: new AppBar(
            title: new Text("New Contact"),
            centerTitle: true,
          ),
          body: new Padding(
            padding: const EdgeInsets.all(20.0),
            child: new Column(
              children: <Widget>[
                new UserForm(
                  isLoading: createUserView.state.userState.isLoading,
                  onSubmit: (User user) async {
                    try {
                      createUserView.setLoadingState(true);
                      var response =
                          await _userRepository.createUser(user: user);
                      if (response) {
                        new CSnackBar(
                          sk: _scaffoldKey,
                          message: "Contact created successfully",
                          action: () =>
                              _scaffoldKey.currentState.hideCurrentSnackBar(),
                        ).show();
                        createUserView.setLoadingState(false);

                        // Update users list
                        var users = await _getUsers();
                        createUserView.setUsers(users);
                      }
                    } on DioError catch (e) {
                      setState(() {
                        _isLoading = false;
                      });
                      print("ERROR: ${e.response}");
                    }
                  },
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}

class CreateUserView {
  final AppState state;
  final void Function(List<User> users) setUsers;
  final void Function(bool isLoading) setLoadingState;

  CreateUserView({this.state, this.setUsers, this.setLoadingState});
}

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';

// Model & Services
import 'package:myh_contact_list/services/internal/models/UserModel.dart';
import 'package:myh_contact_list/services/internal/repositories/UserRepository.dart';

// Widget & Forms
import 'package:myh_contact_list/widgets/CSnackBar.dart';
import 'package:myh_contact_list/forms/UserForm.dart';

// Store
import 'package:myh_contact_list/services/internal/store/states/AppState.dart';
import 'package:myh_contact_list/services/internal/store/actions/UserActions.dart';

class EditUser extends StatefulWidget {
  final User user;

  EditUser({this.user});

  @override
  _EditUserState createState() => _EditUserState();
}

class _EditUserState extends State<EditUser> {
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  UserRepository _userRepository = new UserRepository();

  Future<List<User>> _getUsers({int page = 1, int row = 15}) async {
    try {
      List<User> usersList = [];
      var users = await _userRepository.getUsers(page: page, row: row);
      usersList.addAll(users);
      return usersList;
    } catch (e) {
      throw e;
    }
  }

  @override
  Widget build(BuildContext context) {
    return new StoreConnector<AppState, EditUserView>(
      converter: (store) => EditUserView(
        state: store.state,
        setUsers: (users) => store.dispatch(
          new GetUsersAction(users, reset: true),
        ),
        setLoadingState: (loadingState) => store.dispatch(
          loadingState ? UsersLoadingAction.LOADING : UsersLoadingAction.LOADED,
        ),
      ),
      builder: (BuildContext context, EditUserView editUserView) {
        return new Scaffold(
          key: _scaffoldKey,
          appBar: new AppBar(
            title: new Text("Edit Contact"),
            centerTitle: true,
          ),
          body: new Padding(
            padding: const EdgeInsets.all(20.0),
            child: new Column(
              children: <Widget>[
                new UserForm(
                  isLoading: editUserView.state.userState.isLoading,
                  user: widget.user,
                  onSubmit: (User user) async {
                    try {
                      editUserView.setLoadingState(true);
                      var response = await _userRepository.editUser(user: user);
                      if (response) {
                        new CSnackBar(
                          sk: _scaffoldKey,
                          message: "Contact edited successfully",
                          action: () =>
                              _scaffoldKey.currentState.hideCurrentSnackBar(),
                        ).show();
                        editUserView.setLoadingState(false);

                        // Update users list
                        var users = await _getUsers();
                        editUserView.setUsers(users);
                      }
                    } on DioError catch (e) {
                      editUserView.setLoadingState(false);
                      print("ERROR: ${e.response}");
                    }
                  },
                )
              ],
            ),
          ),
        );
      },
    );
  }
}

class EditUserView {
  final AppState state;
  final void Function(List<User> users) setUsers;
  final void Function(bool isLoading) setLoadingState;

  EditUserView({this.state, this.setUsers, this.setLoadingState});
}

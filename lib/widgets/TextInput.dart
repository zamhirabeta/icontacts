import 'package:flutter/material.dart';

class TextInput extends StatelessWidget {
  final String label;
  final String initialValue;
  final Function onSaved;
  final Function validator;

  TextInput({this.label, this.initialValue = "", this.onSaved, this.validator});

  @override
  Widget build(BuildContext context) {
    return new Padding(
      padding: const EdgeInsets.only(top: 10),
      child: new TextFormField(
        onSaved: onSaved,
        initialValue: initialValue,
        validator: validator,
        decoration: new InputDecoration(
          labelText: label,
          contentPadding: EdgeInsets.only(top: 5, bottom: 3),
        ),
      ),
    );
  }
}

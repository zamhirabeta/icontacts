import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:shared_preferences/shared_preferences.dart';

// Containers & Widgets
import 'package:myh_contact_list/containers/EditUser.dart';
import 'package:myh_contact_list/widgets/CSnackBar.dart';

// Models & Services
import 'package:myh_contact_list/services/internal/models/UserModel.dart';
import 'package:myh_contact_list/services/internal/repositories/UserRepository.dart';

// store
import 'package:myh_contact_list/services/internal/store/states/AppState.dart';
import 'package:myh_contact_list/services/internal/store/actions/UserActions.dart';

class UsersListViewItem extends StatefulWidget {
  final User user;
  final GlobalKey<ScaffoldState> sk;

  UsersListViewItem({this.user, this.sk});

  @override
  _UsersListViewItemState createState() => _UsersListViewItemState();
}

class _UsersListViewItemState extends State<UsersListViewItem> {
  final UserRepository _userRepository = new UserRepository();

  @override
  Widget build(BuildContext context) {
    return new StoreConnector<AppState, ItemView>(
      converter: (store) => ItemView(
        state: store.state,
        setUsers: (users) => store.dispatch(
          new GetUsersAction(users, reset: true),
        ),
        setLoadingState: (loadingState) => store.dispatch(
          loadingState ? UsersLoadingAction.LOADING : UsersLoadingAction.LOADED,
        ),
        toggleFavoriteList: (user) =>
            store.dispatch(ToggleFavoriteListAction(user)),
      ),
      builder: (BuildContext context, ItemView iv) {
        return Slidable(
          actionPane: SlidableDrawerActionPane(),
          actionExtentRatio: 0.25,
          child: new ListTile(
            leading: new CircleAvatar(
              backgroundColor: Colors.indigoAccent,
              child: new Text(
                  widget.user.first_name[0] + widget.user.last_name[0]),
              foregroundColor: Colors.white,
            ),
            title: new Text(
              widget.user.fullName(),
              style: new TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 20,
              ),
            ),
            subtitle: new Text(
              "Tel: ${widget.user.phone_no}",
            ),
            trailing: new IconButton(
              icon: new Icon(
                Icons.grade,
                color: widget.user.isFavorite ? Colors.amber : Colors.grey,
              ),
              onPressed: () async {
                iv.toggleFavoriteList(widget.user);
                await updateOfflineUsersList(iv.state.userState.contacts);
              },
            ),
          ),
          secondaryActions: <Widget>[
            IconSlideAction(
              caption: 'Edit',
              color: Colors.black45,
              icon: Icons.edit,
              onTap: () => Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (context) => EditUser(
                    user: this.widget.user,
                  ),
                ),
              ),
            ),
            IconSlideAction(
              caption: 'Delete',
              color: Colors.red,
              icon: Icons.delete,
              onTap: () => _deleteUser(context, widget.sk),
            ),
          ],
        );
      },
    );
  }

  Future<void> updateOfflineUsersList(List<User> usersList) async {
    SharedPreferences _prefs = await SharedPreferences.getInstance();
    List<String> jsonUsersList = [];
    usersList.forEach((user) {
      jsonUsersList.add(jsonEncode(user.toJson()));
    });
    _prefs.setStringList("users", jsonUsersList);
  }

  Future<List<User>> _getUsers({int page = 1, int row = 15}) async {
    try {
      List<User> usersList = [];
      var users = await _userRepository.getUsers(page: page, row: row);
      usersList.addAll(users);
      return usersList;
    } catch (e) {
      throw e;
    }
  }

  void _deleteUser(BuildContext context, GlobalKey<ScaffoldState> sk) {
    sk.currentState.showSnackBar(
      new SnackBar(
        content: new Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            new Text("Are You sure?"),
            new Row(
              children: <Widget>[
                new StoreConnector<AppState, ItemView>(
                  converter: (store) => ItemView(
                    state: store.state,
                    setUsers: (users) => store.dispatch(
                      new GetUsersAction(users, reset: true),
                    ),
                    setLoadingState: (loadingState) => store.dispatch(
                      loadingState
                          ? UsersLoadingAction.LOADING
                          : UsersLoadingAction.LOADED,
                    ),
                    toggleFavoriteList: (user) =>
                        store.dispatch(ToggleFavoriteListAction(user)),
                  ),
                  builder: (BuildContext context, ItemView iv) {
                    return new FlatButton(
                      onPressed: () async {
                        try {
                          sk.currentState.hideCurrentSnackBar();
                          iv.setLoadingState(true);
                          var response = await _userRepository.deleteUser(
                              id: widget.user.id);
                          iv.setLoadingState(false);
                          if (response) {
                            new CSnackBar(
                              sk: sk,
                              message: "Contact deleted successfully",
                              action: () =>
                                  sk.currentState.hideCurrentSnackBar(),
                            ).show();

                            // Update users list
                            var users = await _getUsers();
                            iv.setUsers(users);
                          }
                        } on DioError catch (e) {
                          iv.setLoadingState(false);
                          print("ERROR: ${e.response}");
                        }
                      },
                      child: new Text("OK"),
                    );
                  },
                ),
                new FlatButton(
                  onPressed: () => sk.currentState.removeCurrentSnackBar(),
                  child: new Text("Cancel"),
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}

class ItemView {
  final AppState state;
  final void Function(List<User> users) setUsers;
  final void Function(bool isLoading) setLoadingState;
  final void Function(User user) toggleFavoriteList;

  ItemView(
      {this.state,
      this.setUsers,
      this.setLoadingState,
      this.toggleFavoriteList});
}

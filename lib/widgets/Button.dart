import 'package:flutter/material.dart';

class Button extends StatelessWidget {
  final String label;
  final Function onTap;
  final bool isLoading;

  Button({@required this.label, this.onTap, this.isLoading = false});

  @override
  Widget build(BuildContext context) {
    return new GestureDetector(
      onTap: !isLoading ? onTap : null,
      child: new Container(
        alignment: Alignment.center,
        decoration: new BoxDecoration(
          color: Colors.blue,
          borderRadius: BorderRadius.all(
            Radius.circular(10),
          ),
        ),
        child: new Padding(
          padding: const EdgeInsets.all(15.0),
          child: !isLoading
              ? new Text(
                  label,
                  textAlign: TextAlign.center,
                  style: new TextStyle(
                    color: Colors.white,
                    fontSize: 16,
                    fontWeight: FontWeight.bold
                  ),
                )
              : new SizedBox(
                  width: 20,
                  height: 20,
                  child: new CircularProgressIndicator(
                    valueColor: new AlwaysStoppedAnimation<Color>(Colors.white),
                    strokeWidth: 2,
                  ),
                ),
        ),
      ),
    );
  }
}

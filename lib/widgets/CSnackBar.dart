import 'package:flutter/material.dart';

class CSnackBar {
  final GlobalKey<ScaffoldState> sk;
  final String message;
  final Function action;
  final String actionLabel;

  CSnackBar({this.sk, this.message, this.action, this.actionLabel = "OK"});

  show() {
    return sk.currentState.showSnackBar(
      new SnackBar(
        content: new Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            new Text(message),
            new FlatButton(
              onPressed: action,
              child: new Text(actionLabel),
            ),
          ],
        ),
      ),
    );
  }
}

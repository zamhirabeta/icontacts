import 'package:flutter/material.dart';

class Loading extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Center(
      child: Column(
        children: <Widget>[
          new CircularProgressIndicator(),
          new Padding(
            padding: const EdgeInsets.only(top: 10),
            child: new Text(
              "Loading...",
              style: new TextStyle(color: Colors.white),
            ),
          ),
        ],
        mainAxisAlignment: MainAxisAlignment.center,
      ),
    );
  }
}

import 'package:flutter/material.dart';

// Models
import 'package:myh_contact_list/services/internal/models/UserModel.dart';

// Widgets
import 'package:myh_contact_list/widgets/UsersListViewItem.dart';

class UsersListView extends StatelessWidget {
  final ScrollController scrollController;
  final List<User> users;
  final GlobalKey<ScaffoldState> sk;

  UsersListView({this.scrollController, this.users, this.sk});

  @override
  Widget build(BuildContext context) {
    return new ListView.separated(
      controller: scrollController,
      itemBuilder: (BuildContext context, int index) {
        return new UsersListViewItem(user: users[index], sk: sk);
      },
      separatorBuilder: (BuildContext context, int index) =>
          new Divider(height: 1),
      itemCount: users.length,
    );
  }
}

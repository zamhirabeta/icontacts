# iContacts

A Flutter project.

## Getting Started

This project is a test app for firstsource.io

## Tasks
- [x] App supports android 6 and above
- [x] List of contacts
- [x] Add new contact (using fab)
- [x] Edit contact (using slide gesture)
- [x] Delete contact (using slide gesture)
- [x] Favorite contacts (can be marked & unmarked with star)
- [x] Favorite contacts list can be accessible now
- [x] Offline usage
- [ ] Drag & Drop in list of favorite contacts
- [ ] Search & filter